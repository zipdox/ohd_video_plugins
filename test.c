#include <dlfcn.h>
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include "ohd_video_plugin.h"

int main(int argc,  char** argv){
    if(argc < 5){
        printf("Usage: ./test [path to .so file] [device #] [hostname] [port] ([width] [height] [fps])\n");
        return 1;
    }
    const char *so_path = argv[1];
    unsigned int dev_num = strtol(argv[2], NULL, 10);
    const char *hostname = argv[3];
    unsigned int port = strtol(argv[4], NULL, 10);
    unsigned int width = 1920, height = 1080, fps = 30;
    if(argc >= 8){
        width = strtol(argv[5], NULL, 10);
        height = strtol(argv[6], NULL, 10);
        fps = strtol(argv[7], NULL, 10);
    }

    void *handle = dlopen(so_path, RTLD_NOW);
    if(handle == 0){
        printf("Invalid path\n");
        return 1;
    }
    ohd_descriptor_func get_descriptor = dlsym(handle, "ohd_descriptor");

    OHDVideoPluginDescriptor *descriptor = get_descriptor();
    OHDVideoPluginHandle *plugin_handle = malloc(sizeof(OHDVideoPluginHandle));
    printf("Platform: %s\n", descriptor->platform);
    descriptor->discover(plugin_handle);

    OHDVideoSource *source = plugin_handle->sources[dev_num];
    printf("Source: %s\n", source->name);

    printf("Initializing stream\n");
    descriptor->source_init(plugin_handle, source, OHD_H264, 5000, 0, OHD_CBR, 30, width, height, fps, hostname, port);
    printf("Starting stream\n");
    descriptor->source_start(plugin_handle, source);
    printf("Press return to exit\n");
    fgetc(stdin);
    printf("Pausing stream\n");
    descriptor->source_pause(plugin_handle, source);
    printf("Closing stream stream\n");
    descriptor->source_close(plugin_handle, source);
}