# OpenHD Video Plugins
This repository contains plugins that provide video capability to OpenHD on specific platforms.

It also contains a standard API for implementing new plugins to add support for platforms.

## API
Each plugin consists of a shared object that contain a function called `ohd_descriptor` that returns a `OHDVideoPluginDescriptor` pointer. See [ohd_video_plugin.h](ohd_video_plugin.h) and the dummy plugin for more details.

## Building
Run `make` in each plugin directory you wish to build. Run `make` in the top directory to build the test binary.

## Testing
Run `./test.sh` to run the UDP receiver. Then run `./test dummy/dummy.so 0 127.0.0.1 5600` (replacing dummy with whatever plugin you're testing) to test the plugin. You can also test the plugin remotely through SSH by replacing `127.0.0.1` with your local IP (and running `./test.sh` locally). Press return to stop the test. Run `./test` without any arguments to see the full syntax.

## Design considerations
- A source must be initialized before being started.
- A source may or may not be paused before being closed.
- Recording may not be available. If so, the function for it must be NULL.
- A source may be paused and/or stopped before recording is stopped. A plugin must deal wit this.
- The consumer of the plugins must take the maximum resolution and framerate into account when initializing a source.
- All calls to plugin methods must block until they are fully completed. If blocking is not permissible for the cunsumer, they must implement threading.
