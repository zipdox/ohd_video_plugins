CC=gcc
LIBS=-ldl
BINARY=test
OPTS=-Wall -fpic

all: $(BINARY)

$(BINARY): test.c
	gcc -o $@ $< -ldl $(LIBS)

clean:
	rm $(BINARY)
