#include "../ohd_video_plugin.h"
#include <gst/gst.h>

typedef struct {
    GstElement *pipeline;
    GMainLoop *loop;
    GMutex running_mutex;
} SourceThread;

static int discover(OHDVideoPluginHandle *plugin){
    plugin->sources = g_new0(OHDVideoSource*, 2);
    plugin->sources[1] = NULL;
    OHDVideoSource *source = g_new0(OHDVideoSource, 1);
    plugin->sources[0] = source;
    source->name = "videotestsrc";
    source->max_width = 1920;
    source->max_height = 1080;
    source->max_fps = 30;
    return 1;
}

static GstBusSyncReply source_msg_handler(GstBus *bus, GstMessage *message, gpointer user_data){
    SourceThread *thread = user_data;
    if(GST_MESSAGE_TYPE(message) == GST_MESSAGE_EOS){
        g_main_loop_quit(thread->loop);
    }
    return GST_BUS_PASS;
}

static gpointer thread_run(gpointer user_data){
    SourceThread *thread = user_data;
    g_mutex_lock(&thread->running_mutex);
    g_main_loop_run(thread->loop);
    g_mutex_unlock(&thread->running_mutex);
    return NULL;
}

static const char *encoder_names(OHDVideoCodec codec){
    switch(codec){
        case OHD_H264: return "x264enc";
        case OHD_H265: return "x265enc";
        case OHD_MJPEG: return "jpegenc";
        default: return NULL;
    }
}

static const char *parser_names(OHDVideoCodec codec){
    switch(codec){
        case OHD_H264: return "h264parse";
        case OHD_H265: return "h265parse";
        case OHD_MJPEG: return "jpegparse";
        default: return NULL;
    }
}

static const char *payloader_names(OHDVideoCodec codec){
    switch(codec){
        case OHD_H264: return "rtph264pay";
        case OHD_H265: return "rtph265pay";
        case OHD_MJPEG: return "rtpjpegpay";
        default: return NULL;
    }
}

static int rate_control_int(OHDRateControl mode){
    switch(mode){
        case OHD_VBR: return 17;
        case OHD_CRF: return 5;
        case OHD_CBR:
        default: return 0;
    }
}

static gint source_init(
    OHDVideoPluginHandle *plugin, OHDVideoSource *source,
    OHDVideoCodec codec, unsigned int kbps_or_quality,
    unsigned int cq, OHDRateControl rate_control, unsigned int key_int,
    unsigned int width, unsigned int height, unsigned int fps,
    const char *hostname, unsigned int port
){
    gst_init(NULL, NULL);

    GstElement *pipeline, *src, *encoder, *queue, *parser, *payloader, *udpsink;

    pipeline = gst_pipeline_new(NULL);

    src = gst_element_factory_make("videotestsrc", NULL);
    encoder = gst_element_factory_make(encoder_names(codec), NULL);
    if(codec == OHD_MJPEG){
        g_object_set(G_OBJECT(encoder), "quality", (gint) kbps_or_quality, NULL);
    }else{
        g_object_set(G_OBJECT(encoder),
            "bitrate", (guint) kbps_or_quality,
            "quantizer", (guint) cq,
            "pass", rate_control_int(rate_control),
            "speed-preset", 1,
            "tune", 0x00000004,
            "key-int-max", (guint) key_int,
            NULL
        );
    }
    queue = gst_element_factory_make("queue", NULL);
    parser = gst_element_factory_make(parser_names(codec), NULL);
    if(codec != OHD_MJPEG){
        g_object_set(G_OBJECT(parser), "config-interval", (gint) -1, NULL);
    }
    payloader = gst_element_factory_make(payloader_names(codec), NULL);
    g_object_set(G_OBJECT(payloader), "mtu", (guint) 1024, NULL);
    udpsink = gst_element_factory_make("udpsink", NULL);
    g_object_set(G_OBJECT(udpsink),
        "host", hostname,
        "port", (gint) port,
        NULL
    );

    gst_bin_add_many(GST_BIN(pipeline), src, encoder, queue, parser, payloader, udpsink, NULL);

    GstCaps *caps = gst_caps_new_simple("video/x-raw",
        "format", G_TYPE_STRING, "I420",
        "width", G_TYPE_INT, width,
        "height", G_TYPE_INT, height,
        "framerate", GST_TYPE_FRACTION, fps, 1,
        NULL
    );
    g_assert_true(gst_element_link_filtered(src, encoder, caps));

    g_assert_true(gst_element_link_many(encoder, queue, parser, payloader, udpsink, NULL));

    SourceThread *thread = g_new0(SourceThread, 1);
    source->private_data = thread;
    thread->pipeline = pipeline;
    thread->loop = g_main_loop_new(NULL, FALSE);
    g_mutex_init(&thread->running_mutex);

    GstBus *pipeline_bus = gst_element_get_bus(thread->pipeline);
    gst_bus_set_sync_handler(pipeline_bus, source_msg_handler, thread, NULL);
    gst_object_unref(pipeline_bus);

    GThread *run_thread = g_thread_new(NULL, thread_run, thread);
    g_thread_unref(run_thread);

    return source->private_data != NULL;
}

static void source_start(OHDVideoPluginHandle *plugin, OHDVideoSource *source){
    SourceThread *thread = source->private_data;
    gst_element_set_state(GST_ELEMENT(thread->pipeline), GST_STATE_PLAYING);
}

static void source_pause(OHDVideoPluginHandle *plugin, OHDVideoSource *source){
    SourceThread *thread = source->private_data;
    gst_element_set_state(thread->pipeline, GST_STATE_PAUSED);
}

static void source_close(OHDVideoPluginHandle *plugin, OHDVideoSource *source){
    SourceThread *thread = source->private_data;
    gst_element_set_state(GST_ELEMENT(thread->pipeline), GST_STATE_PLAYING); // required to process EOS
    gst_element_send_event(thread->pipeline, gst_event_new_eos());
    g_mutex_lock(&thread->running_mutex);
    g_mutex_unlock(&thread->running_mutex);
    g_mutex_clear(&thread->running_mutex);
    gst_element_set_state(GST_ELEMENT(thread->pipeline), GST_STATE_NULL);
    gst_object_unref(thread->pipeline);
    g_free((gpointer) thread);
}

static void cleanup(OHDVideoPluginHandle *plugin){
    g_free((gpointer) plugin->sources[0]);
    g_free((gpointer) plugin->sources);
}

static const OHDVideoPluginDescriptor descriptor = {
    "dummy",
    discover,
    source_init,
    source_start,
    source_pause,
    NULL,
    NULL,
    source_close,
    cleanup
};

extern const OHDVideoPluginDescriptor *ohd_descriptor(){
    return &descriptor;
}
