#include "../ohd_video_plugin.h"
#include <gst/gst.h>
#include <sys/ioctl.h>
#include <fcntl.h>
#include <linux/videodev2.h>
#include <libv4l2.h>

typedef enum {
    RK_HDMI,
    RK_ISPv6
} SourceType;

typedef struct {
    const char *dev_path;
    SourceType type;

    GstElement *pipeline;
    GstElement *src;
    GstElement *encoder;
    GstElement *queue;
    GstElement *parser;
    GstElement *payloader;
    GstElement *udpsink;
    GMainLoop *loop;
    GMutex running_mutex;
} SourceObj;

static int discover(OHDVideoPluginHandle *plugin){
    plugin->sources = NULL;
    int n_devices = 0;

    GError *error = NULL;
    GDir *dir = g_dir_open("/dev/", 0, &error);
    if(error){
        g_print("Error reading devices: %s\n", error->message);
        g_error_free(error);
        return 0;
    }

    const char *dev_name;
    const char *dev_path;
    int dev_fd;

    while((dev_name = g_dir_read_name(dir))){
        if(!g_str_has_prefix(dev_name, "video")) continue;
        if(g_str_has_prefix(dev_name, "video-")) continue;
        dev_path = g_build_filename("/dev", dev_name, NULL);
        g_print("Scanning %s\n", dev_path);

        dev_fd = open(dev_path, O_RDWR);
        if(dev_fd < 0){
            g_print("Error opening %s\n", dev_path);
            g_free((gpointer) dev_path);
            continue;
        }

        struct v4l2_capability caps = {};
        OHDVideoSource *source;
        SourceObj *source_obj;
        if (ioctl(dev_fd, VIDIOC_QUERYCAP, &caps) == -1) {
           g_print("Capability query failed for %s", dev_path);
        }else{
            if(g_strcmp0((const char*) caps.driver, "rk_hdmirx") == 0){
                n_devices++;
                plugin->sources = g_realloc_n(plugin->sources, n_devices + 1, sizeof(OHDVideoSource*));
                plugin->sources[n_devices] = NULL;
                source = g_new0(OHDVideoSource, 1);
                plugin->sources[n_devices-1] = source;
                source->name = g_strdup("Rockchip HDMI in");
                g_print("Found {%s} @ %s\n", source->name, dev_path);
                source->max_width = 1920;
                source->max_height = 1080;
                source->max_fps = 60;

                source_obj = g_new0(SourceObj, 1);
                source->private_data = source_obj;

                source_obj->dev_path = g_strdup(dev_path);
                source_obj->type = RK_HDMI;
            }else if(g_strcmp0((const char*) caps.driver, "rkisp_v6") == 0 && g_strcmp0((const char*) caps.card, "rkisp_mainpath") == 0){
                n_devices++;
                plugin->sources = g_realloc_n(plugin->sources, n_devices + 1, sizeof(OHDVideoSource*));
                plugin->sources[n_devices] = NULL;
                source = g_new0(OHDVideoSource, 1);
                plugin->sources[n_devices-1] = source;
                source->name = g_strdup_printf("Rockchip ISPv6 [%s]", dev_name + 5);
                g_print("Found {%s} @ %s\n", source->name, dev_path);
                source->max_width = 1920;
                source->max_height = 1080;
                source->max_fps = 60;

                source_obj = g_new0(SourceObj, 1);
                source->private_data = source_obj;

                source_obj->dev_path = g_strdup(dev_path);
                source_obj->type = RK_ISPv6;
            }
        }

        close(dev_fd);
        g_free((gpointer) dev_path);
    }
    g_dir_close(dir);

    return 1;
}

static GstBusSyncReply source_msg_handler(GstBus *bus, GstMessage *message, gpointer user_data){
    SourceObj *source_obj = user_data;
    if(GST_MESSAGE_TYPE(message) == GST_MESSAGE_EOS){
        g_main_loop_quit(source_obj->loop);
    }
    return GST_BUS_PASS;
}

static gpointer thread_run(gpointer user_data){
    SourceObj *source_obj = user_data;
    g_mutex_lock(&source_obj->running_mutex);
    g_main_loop_run(source_obj->loop);
    g_mutex_unlock(&source_obj->running_mutex);
    return NULL;
}

static const char *encoder_names(OHDVideoCodec codec){
    switch(codec){
        case OHD_H264: return "mpph264enc";
        case OHD_H265: return "mpph265enc";
        case OHD_MJPEG: return "mppjpegenc";
        default: return NULL;
    }
}

static const char *parser_names(OHDVideoCodec codec){
    switch(codec){
        case OHD_H264: return "h264parse";
        case OHD_H265: return "h265parse";
        case OHD_MJPEG: return "jpegparse";
        default: return NULL;
    }
}

static const char *payloader_names(OHDVideoCodec codec){
    switch(codec){
        case OHD_H264: return "rtph264pay";
        case OHD_H265: return "rtph265pay";
        case OHD_MJPEG: return "rtpjpegpay";
        default: return NULL;
    }
}

static int rate_control_int(OHDRateControl mode){
    switch(mode){
        case OHD_VBR: return 0;
        case OHD_CBR: return 1;
        case OHD_CRF: return 2;
        default: return 0;
    }
}

static const int h264_level_nums[] = {
    10,
    99,
    11,
    12,
    13,
    20,
    21,
    22,
    30,
    31,
    32,
    40,
    41,
    42,
    50,
    51,
    52
};

static const int h264_level_macroblocks[] = {
    1485,
    1485,
    3000,
    6000,
    11880,
    11880,
    19800,
    20250,
    40500,
    108000,
    216000,
    245760,
    245760,
    522240,
    589824,
    983040,
    2073600
};

static int h264_level(int height, int width, int fps){
    int macroblocks = width * height * fps / 256;
	int best_level = 0;
	for(int i = 0; i < sizeof(h264_level_nums)/sizeof(int); i++){
		if (macroblocks < h264_level_macroblocks[i]) {
			best_level = h264_level_nums[i];
			break;
		}
	}
    return best_level;
}

static gint source_init(
    OHDVideoPluginHandle *plugin, OHDVideoSource *source,
    OHDVideoCodec codec, unsigned int kbps_or_quality,
    unsigned int cq, OHDRateControl rate_control, unsigned int key_int,
    unsigned int width, unsigned int height, unsigned int fps,
    const char *hostname, unsigned int port
){
    gst_init(NULL, NULL);

    SourceObj *source_obj = source->private_data;

    source_obj->pipeline = gst_pipeline_new(NULL);

    source_obj->src = gst_element_factory_make("v4l2src", NULL);
    g_object_set(G_OBJECT(source_obj->src),
        "device", source_obj->dev_path,
        "do-timestamp", TRUE,
        NULL
    );
    source_obj->encoder = gst_element_factory_make(encoder_names(codec), NULL);
    if(codec == OHD_MJPEG){
        g_object_set(G_OBJECT(source_obj->encoder), "quality", (gint) kbps_or_quality, NULL);
    }else{
        g_object_set(G_OBJECT(source_obj->encoder),
            "bps", (guint) kbps_or_quality,
            "qp-init", (guint) cq,
            "rc-mode", rate_control_int(rate_control),
            "gop", (gint) key_int,
            "width", (guint) width,
            "height", (gint) height,
            NULL
        );
        if(codec == OHD_H264) g_object_set(G_OBJECT(source_obj->encoder),
            "level", (guint) h264_level(height, width, fps),
            NULL
        );
    }
    source_obj->queue = gst_element_factory_make("queue", NULL);
    source_obj->parser = gst_element_factory_make(parser_names(codec), NULL);
    if(codec != OHD_MJPEG){
        g_object_set(G_OBJECT(source_obj->parser), "config-interval", (gint) -1, NULL);
    }
    source_obj->payloader = gst_element_factory_make(payloader_names(codec), NULL);
    g_object_set(G_OBJECT(source_obj->payloader), "mtu", (guint) 1024, NULL);
    source_obj->udpsink = gst_element_factory_make("udpsink", NULL);
    g_object_set(G_OBJECT(source_obj->udpsink),
        "host", hostname,
        "port", (gint) port,
        NULL
    );

    gst_bin_add_many(GST_BIN(source_obj->pipeline),
        source_obj->src,
        source_obj->encoder,
        source_obj->queue,
        source_obj->parser,
        source_obj->payloader,
        source_obj->udpsink,
        NULL
    );

    GstCaps *caps;
    switch(source_obj->type){
        case RK_HDMI: {
            caps = gst_caps_new_simple("video/x-raw",
                "framerate", GST_TYPE_FRACTION, fps, 1,
                NULL
            );
            break;
        }
        case RK_ISPv6:
        default: {
            caps = gst_caps_new_simple("video/x-raw",
                "format", G_TYPE_STRING, "NV12",
                "framerate", GST_TYPE_FRACTION, fps, 1,
                NULL
            );
            break;
        }
    }
    g_assert_true(gst_element_link_filtered(source_obj->src, source_obj->encoder, caps));

    g_assert_true(gst_element_link_many(
        source_obj->encoder,
        source_obj->queue,
        source_obj->parser,
        source_obj->payloader,
        source_obj->udpsink,
        NULL
    ));

    source_obj->loop = g_main_loop_new(NULL, FALSE);
    g_mutex_init(&source_obj->running_mutex);

    GstBus *pipeline_bus = gst_element_get_bus(source_obj->pipeline);
    gst_bus_set_sync_handler(pipeline_bus, source_msg_handler, source_obj, NULL);
    gst_object_unref(pipeline_bus);

    GThread *run_thread = g_thread_new(NULL, thread_run, source_obj);
    g_thread_unref(run_thread);

    return source_obj->pipeline != NULL;
}

static void source_start(OHDVideoPluginHandle *plugin, OHDVideoSource *source){
    SourceObj *source_obj = source->private_data;
    gst_element_set_state(GST_ELEMENT(source_obj->pipeline), GST_STATE_PLAYING);
}

static void source_pause(OHDVideoPluginHandle *plugin, OHDVideoSource *source){
    SourceObj *source_obj = source->private_data;
    gst_element_set_state(GST_ELEMENT(source_obj->pipeline), GST_STATE_PAUSED);
}

static void source_close(OHDVideoPluginHandle *plugin, OHDVideoSource *source){
    SourceObj *source_obj = source->private_data;
    gst_element_set_state(GST_ELEMENT(source_obj->pipeline), GST_STATE_PLAYING); // required to process EOS
    gst_element_send_event(source_obj->pipeline, gst_event_new_eos());
    g_mutex_lock(&source_obj->running_mutex); // it'll lock then the loop thread finishes and unlocks the mutex
    g_mutex_unlock(&source_obj->running_mutex); // so we block the calling thread until that happens
    g_mutex_clear(&source_obj->running_mutex);
    gst_element_set_state(GST_ELEMENT(source_obj->pipeline), GST_STATE_NULL);
    gst_object_unref(source_obj->pipeline);
}

static void cleanup(OHDVideoPluginHandle *plugin){
    SourceObj *source_obj;
    OHDVideoSource *source;
    for(int i = 0;; i++){
        source = plugin->sources[i];
        if(!source) break;
        source_obj = source->private_data;
        g_free((gpointer) source_obj->dev_path);
        g_free((gpointer) source_obj);
    }
    g_free((gpointer) plugin->sources);
}

static const OHDVideoPluginDescriptor descriptor = {
    "rockchip",
    discover,
    source_init,
    source_start,
    source_pause,
    NULL,
    NULL,
    source_close,
    cleanup
};

extern const OHDVideoPluginDescriptor *ohd_descriptor(){
    return &descriptor;
}
