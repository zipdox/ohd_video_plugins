typedef struct {
    const char *name;
    unsigned int max_width;
    unsigned int max_height;
    unsigned int max_fps;
    void *private_data; // optional pointer to store custom data structure for whatever the source needs
} OHDVideoSource;

typedef struct {
    OHDVideoSource **sources; // null terminated list
    void *private_data; // optional pointer to store custom data structure for whatever the plugin needs globally
} OHDVideoPluginHandle;

typedef enum {
    OHD_H264,
    OHD_H265,
    OHD_MJPEG
} OHDVideoCodec;

typedef enum {
    OHD_CBR,
    OHD_VBR,
    OHD_CRF
} OHDRateControl;

typedef struct {
    const char *platform;
    int (*discover)(OHDVideoPluginHandle *plugin);
    int (*source_init)(OHDVideoPluginHandle *plugin, OHDVideoSource *source,
        OHDVideoCodec codec, unsigned int kbps_or_quality, // bitrate in kbps or quality in percent
        unsigned int cq, OHDRateControl rate_control, unsigned int key_int,
        unsigned int width, unsigned int height, unsigned int fps,
        const char *hostname, unsigned int port);
    void (*source_start)(OHDVideoPluginHandle *plugin, OHDVideoSource *source);
    void (*source_pause)(OHDVideoPluginHandle *plugin, OHDVideoSource *source);
    // can be null if there is no (implemented) recording capability
    void (*source_record_start)(OHDVideoPluginHandle *plugin, OHDVideoSource *source,
        const char *path, unsigned int width, unsigned int height, // resolution will be ignored if the plugin doesn't support simultaneous encoding
        OHDVideoCodec codec, unsigned int kbps_or_quality, // will also be ignored in this scenario
        unsigned int cq, OHDRateControl rate_control, unsigned int key_int);
    // will also be null in case of the above scenario
    void (*source_record_stop)(OHDVideoPluginHandle *plugin, OHDVideoSource *source);
    void (*source_close)(OHDVideoPluginHandle *plugin, OHDVideoSource *source);
    void (*cleanup)(OHDVideoPluginHandle *plugin);
} OHDVideoPluginDescriptor;

typedef OHDVideoPluginDescriptor *(*ohd_descriptor_func)();
